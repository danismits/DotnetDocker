# ----------------------------------
# Pterodactyl Core Dockerfile
# Environment: NET Core
# Minimum Panel Version: 0.6.0
# ----------------------------------
FROM ubuntu:16.04

RUN apt-get update \
    && apt-get -y upgrade \
    && apt-get install -y -q --no-install-recommends apt-utils dialog \
    curl \    
    ca-certificates \
    krb5-locales \
    krb5-user \
    apt-transport-https \
    libunwind8 \
    liblttng-ust0 \
    libcurl3 \
    libssl1.0.0 \
    libuuid1 \
    libkrb5-3 \
    zlib1g \
    libicu55 \
	sudo \
    && useradd -d /home/container -m container

RUN sudo curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
RUN sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg

RUN sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/microsoft-ubuntu-xenial-prod xenial main" > /etc/apt/sources.list.d/dotnetdev.list'
RUN sudo apt-get update \
    && sudo apt-get install -y --no-install-recommends \
    dotnet-sdk-2.1.4 \
    dotnet --version

USER 	container
ENV  	USER container
ENV  	HOME /home/container
WORKDIR	/home/container

COPY ./entrypoint.sh /entrypoint.sh
CMD ["/bin/bash", "/entrypoint.sh"]